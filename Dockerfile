# Stage 0, "build-stage", based on Node.js, to build and compile Angular
FROM node:10 as build-stage

RUN mkdir -p /app

WORKDIR /app

COPY package*.json /app/

RUN npm install --silent

COPY ./ /app/

RUN npm run build -- --output-path=./dist/out --prod

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.15

COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 4000
