import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromAuth from "./service-layer/store/auth.reducer";
import * as fromAuthActions from "./service-layer/store/auth.actions";
import { takeWhile } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'nuit-info';
  componentActive: boolean = true;
  estConnecte: boolean = false;

  constructor(
    private store: Store<fromAuth.AuthState>
  ) {}


  ngOnInit() {
    this.store.pipe(select(fromAuth.getEstConnecte),
      takeWhile(() => this.componentActive))
      .subscribe(
        statut => {
          this.estConnecte = statut;
        }
      )
  }


  ngOnDestroy() {
    this.componentActive = false;
  }

}
