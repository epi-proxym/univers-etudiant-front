import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './UI/features/views/home/home.component';
import { WizardComponent } from './UI/features/views/wizard/wizard.component';
import { ProfileComponent } from './UI/features/profile/profile.component';


const routes: Routes = [

  {
    path: '',
    redirectTo: 'auth/connexion',
    pathMatch: 'full'
  },

  {
    path: 'home',
    component: HomeComponent
  },

  {
    path: 'auth',
    loadChildren: './UI/features/auth/auth.module#AuthModule'
  },

  {
    path: 'wizard',
    component: WizardComponent
  },

  {
    path: 'profile',
    component: ProfileComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
