import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ClrWizard } from '@clr/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as fromAuthState from '../../../../service-layer/store/auth.reducer';
import * as fromAuthActions from '../../../../service-layer/store/auth.actions';
import { Store, select } from '@ngrx/store';
import { AuthService } from '../../auth/auth.service';
import { takeWhile } from 'rxjs/operators';
import { User } from 'src/app/business-layer/user.model';


@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.css']
})
export class WizardComponent implements OnInit, OnDestroy {

  form: FormGroup;
  formCv: FormGroup;
  constructor(
    private router: Router,
    private store: Store<fromAuthState.AuthState>,
    private authService: AuthService
  ) { }

  @ViewChild("wizard",{static: false}) wizard: ClrWizard;
  skipStepTwo: boolean = true;
  _open: boolean = true;
  utilisateur: User;
  componentActive: boolean = true;


  ngOnInit(){
    this.store.pipe(select(fromAuthState.getUser),
      takeWhile(() => this.componentActive))
      .subscribe(
        user => {
          this.utilisateur = user;
        }
      )
      
    this.form = new FormGroup({
      sexe: new FormControl(null , {validators: [Validators.required]}),
      nationalite: new FormControl(null , {validators: [Validators.required]})
      
    })

    this.formCv = new FormGroup({
      cv: new FormControl(null , {validators: [Validators.required]})
    })

  }



  fileChange(event): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
        const file = fileList[0];

        const formData = new FormData();
        formData.append('file', file, file.name);

       
    }
}


  toggleStepTwo() {
      this.skipStepTwo = !this.skipStepTwo;
  }

  open() {
      this._open = !this.open;
  }


  envoyerDonnees(){

    this.utilisateur.nationalite = this.form.value.nationalite
    this.utilisateur.sexe = this.form.value.sexe;
    this.store.dispatch(new fromAuthActions.ModifierProfil(this.utilisateur));
    this.router.navigate(["home"]);
  }


  ngOnDestroy() {
    this.componentActive = false;
  }
}
