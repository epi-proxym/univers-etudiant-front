import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromAuthState from '../../../../service-layer/store/auth.reducer';
import * as fromAuthActions from '../../../../service-layer/store/auth.actions';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  form: FormGroup;

  constructor(
    private store: Store<fromAuthState.AuthState>,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      matricule: new FormControl(null , {validators: [Validators.required]}),
      nom: new FormControl(null , {validators: [Validators.required]}),
      prenom: new FormControl(null , {validators: [Validators.required]}),
      email: new FormControl(null , {validators: [Validators.required, Validators.email]}),
      password: new FormControl(null , {validators: [Validators.required]})
    })
  }



  inscription(){
    const info = {
      lastName: this.form.value.nom,
      firstName: this.form.value.prenom,
      phoneNumber: this.form.value.matricule,
      email: this.form.value.email,
      password: this.form.value.password
    }

    this.authService.inscription(info);


  }
  }
