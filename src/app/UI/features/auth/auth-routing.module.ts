import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';


const authRoutes: Routes = [
    {
        path: 'connexion',
        component: ConnexionComponent
    },
    {
        path: 'inscription',
        component: InscriptionComponent
    }
];



@NgModule({
    imports: [
        RouterModule.forChild(authRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule { }
