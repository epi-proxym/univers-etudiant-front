import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducer } from 'src/app/service-layer/Store/auth.reducer';
import { AuthEffects } from 'src/app/service-layer/Store/auth.effects';
import { HttpClientModule } from '@angular/common/http';

import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { AuthRoutingModule } from './auth-routing.module';



@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ClarityModule,
        RouterModule,
        HttpClientModule,
        AuthRoutingModule,
        StoreModule.forFeature('auth', reducer),
        EffectsModule.forFeature([AuthEffects])
    ],
    exports: [],
    declarations: [
    ConnexionComponent,
    InscriptionComponent],
    providers: [],
})
export class AuthModule { }
