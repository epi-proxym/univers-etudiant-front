import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import * as fromAuth from '../../../../service-layer/store/auth.reducer';
import * as fromAuthActions from '../../../../service-layer/store/auth.actions';
import { takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit, OnDestroy {

  form: FormGroup;
  componentActive: boolean = true;

  constructor(
    private authService: AuthService,
    private store: Store<fromAuth.AuthState>,
    private router: Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null , {validators: [Validators.required, Validators.email ]}),
      password: new FormControl(null , {validators: [Validators.required]})
    })

    this.store.pipe(select(fromAuth.getEstConnecte),
      takeWhile(() => this.componentActive))
      .subscribe(
        statut => {
          if (statut) {
            this.router.navigate(["wizard"]);
          }
        }
      )
  }



  connexion(){
    const info = {
      email: this.form.value.email,
      password: this.form.value.password
    }

    this.store.dispatch(new fromAuthActions.Connexion(info));


  }


  ngOnDestroy(){
    this.componentActive = false;
  }




}
