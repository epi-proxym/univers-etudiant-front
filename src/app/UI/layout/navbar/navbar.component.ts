import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/business-layer/user.model';
import { Store, select } from '@ngrx/store';
import { takeWhile } from 'rxjs/operators';
import * as fromAuth from "../../../service-layer/store/auth.reducer";
import * as fromAuthActions from "../../../service-layer/store/auth.actions";

import { Router } from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  utilisateur: User;
  componentActive: boolean = true;

  constructor(
    private store: Store<fromAuth.AuthState>,
    private router: Router
  ) { }

  ngOnInit() {

    this.store.pipe(select(fromAuth.getUser),
      takeWhile(() => this.componentActive))
      .subscribe(
        user => {
          this.utilisateur = user;
        }
      )

      this.store.pipe(select(fromAuth.getEstConnecte),
      takeWhile(() => this.componentActive))
      .subscribe(
        statut => {
          if (!statut) {
            this.router.navigate(["auth/connexion"]);
          }
        }
      )
  }


  deconnexion(){
    this.store.dispatch(new fromAuthActions.Deconnexion())
  }


  ngOnDestroy() {
    this.componentActive = false;
    
  }

}
