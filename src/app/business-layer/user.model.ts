export interface User {
    mat: String,
    nom: String,
    prenom: String,
    email: String,
    nationalite: String,
    sexe: String,
    avatar: String,
    cv: any
}