import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAuthActions from './auth.actions'
import { User } from 'src/app/business-layer/user.model';


export interface AuthState {
    user: User | null,
    token: string
    estConnecte: boolean,
    erreur: any
}



const initialState: AuthState = {
    user: null,
    token: '',
    estConnecte: false,
    erreur: ''
};



// Création des selecteurs pour tous les states
const getAuthFeatureState = createFeatureSelector<AuthState>('auth');

// Création des selecteurs pour chaque propriété de state
export const getUser = createSelector(
    getAuthFeatureState,
    state => state.user
);

export const getEstConnecte = createSelector(
    getAuthFeatureState,
    state => state.estConnecte
);



export const getToken = createSelector(
    getAuthFeatureState,
    state => state.token
);



export const getErreur = createSelector(
    getAuthFeatureState,
    state => state.erreur
);



export function reducer(state = initialState, action: fromAuthActions.AuthActions): AuthState {

    switch(action.type) {
        

       case fromAuthActions.AuthActionsTypes.ConnexionSuccess:
            return {
                ...state,
                estConnecte: true,
                user: action.payload.user,
                token: action.payload.token,
                erreur: ''
            };

        case fromAuthActions.AuthActionsTypes.ConnexionFail:
            return {
                ...state,
                estConnecte: false,
                erreur: action.payload
            };

        

       

        case fromAuthActions.AuthActionsTypes.Deconnexion:
            return {
                ...state,
                estConnecte: false,
               user: null,
                token: '',
                erreur: ''
            }


        case fromAuthActions.AuthActionsTypes.ModifierProfilSuccess:
            return {
                ...state,
                user: action.payload
            }


        case fromAuthActions.AuthActionsTypes.ModifierProfilFail:
            return {
                ...state,
                erreur: action.payload
            }    

        

        default:
            return state;
    }




}