import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthService } from 'src/app/UI/Features/auth/auth.service';
import * as fromAuthActions from './auth.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';




@Injectable()
export class AuthEffects {

    constructor(
        private actions$: Actions,
        private authService: AuthService
    ) { }

/*
    @Effect()
    register$ = this.actions$.pipe(
        ofType(fromAuthActions.AuthActionsTypes.Inscription),
        map((action: fromAuthActions.Inscription) => action.payload),
        mergeMap((trainee) => this.authService.registerTrainee(trainee).pipe(
            map((result: any) => {

                    console.log("Register effect success");
                    return new fromAuthActions.InscriptionSuccess(true);

            }),
            catchError(err => of(new fromAuthActions.InscriptionFail(err)))
        ))
    );

*/


@Effect()
connexion$ = this.actions$.pipe(
    ofType(fromAuthActions.AuthActionsTypes.Connexion),
    map((action: fromAuthActions.Connexion) => action.payload),
    mergeMap((user) => this.authService.connexion(user).pipe(
        map((result: any) => {

                console.log("Register effect success");
                return new fromAuthActions.ConnexionSuccess(result);

        }),
        catchError(err => of(new fromAuthActions.ConnexionFail(err)))
    ))
);



@Effect()
modifierprofile$ = this.actions$.pipe(
    ofType(fromAuthActions.AuthActionsTypes.ModifierProfil),
    map((action: fromAuthActions.ModifierProfil) => action.payload),
    mergeMap((user) => this.authService.modifierProfil(user).pipe(
        map((result: any) => {

                console.log("Register effect success");
                return new fromAuthActions.ModifierProfilSuccess(result);

        }),
        catchError(err => of(new fromAuthActions.ModifierProfilFail(err)))
    ))
);


}