import { Action } from '@ngrx/store';
import { User } from 'src/app/business-layer/user.model';


export enum AuthActionsTypes {
    Connexion = '[Auth] Connexion',
    ConnexionSuccess = '[Auth] Connexion Success',
    ConnexionFail = '[Auth] Connexion Fail',
    ResetMdp = '[Auth] Reset Mot de passe',
    ResendVerifEmail = '[Auth] Resend Verification Email',
    Deconnexion = '[Auth] Deconnexion',
    ModifierProfil = '[Auth] Modifier profile',
    ModifierProfilSuccess = '[Auth] Modifier profile Success',
    ModifierProfilFail = '[Auth] Modifier profile Fail'
}






// Connexion

export class Connexion implements Action {
    readonly type = AuthActionsTypes.Connexion;

    constructor(public payload: any) { }
}

export class ConnexionSuccess implements Action {
    readonly type = AuthActionsTypes.ConnexionSuccess;

    constructor(public payload: any) { }
}

export class ConnexionFail implements Action {
    readonly type = AuthActionsTypes.ConnexionFail;

    constructor(public payload: any) { }
}









export class ResenedVerifEmail implements Action {
    readonly type = AuthActionsTypes.ResendVerifEmail;

    constructor(public payload: boolean) { }
}


export class Deconnexion implements Action {
    readonly type = AuthActionsTypes.Deconnexion;
}



export class ModifierProfil implements Action {
    readonly type = AuthActionsTypes.ModifierProfil;

    constructor(public payload: any) { }
}

export class ModifierProfilSuccess implements Action {
    readonly type = AuthActionsTypes.ModifierProfilSuccess;

    constructor(public payload: any) { }
}

export class ModifierProfilFail implements Action {
    readonly type = AuthActionsTypes.ModifierProfilFail;

    constructor(public payload: any) { }
}





export type AuthActions = Connexion | ConnexionSuccess | ConnexionFail | 
     ResenedVerifEmail | Deconnexion | ModifierProfil |ModifierProfilSuccess |ModifierProfilFail